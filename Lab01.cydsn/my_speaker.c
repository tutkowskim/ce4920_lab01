#include <device.h>
#include "speaker.h"


/*******************************************************************************
* Function Name: Speaker_Play_Tone
********************************************************************************
* Summary:
*  Play welcome music through the speaker
*
* Parameters:
*  double freq - The frequncy of the note in Hertz
*  double length - The length of the note in secounds
*
* Return:
*  None
*
*******************************************************************************/
void Speaker_Play_Tone(double freq, double length) {
    double i;
	for(i = 0; i < freq * length; ++i) {
		Speaker_Write(0);
		CyDelayUs((uint32)(1.0/(2 * freq) * 1000000));
		Speaker_Write(1);
		CyDelayUs((uint32)(1.0/(2 * freq) * 1000000));
	}
}


/*******************************************************************************
* Function Name: Speaker_Play_Welcome_Music
********************************************************************************
* Summary:
*  Play welcome music through the speaker
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void Speaker_Play_Welcome_Music() {
	double tones[] = {
		523.25, 783.99, 659.25, 440,493.88, 466.16,
		440, 783.99, 659.25, 783.99, 440, 698.46,
		783.99, 659.25, 523.25, 587.33, 493.88};
	
	uint8 i;
	for(i = 0; i < sizeof(tones)/sizeof(tones[0]); ++i) {
		Speaker_Play_Tone(tones[i], .25);
		CyDelay(40);
	}
}
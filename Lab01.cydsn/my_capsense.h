#ifndef MY_CAPSENSE_H
#define MY_CAPSENSE_H

#include <device.h>
	
#define ON         	(1)
#define OFF         (0)
#define NO_FINGER   (0xFF)
#define NO_SWIPE 	(0)
#define LEFT_SWIPE 	(1)
#define RIGHT_SWIPE (2)
	
int Cap_Sensor_Swipe_Detected();
int Cap_Sensor_Button_Status(uint8 button);

#endif
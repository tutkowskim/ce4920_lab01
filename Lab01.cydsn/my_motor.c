#include <device.h>
#include "my_motor.h"

uint32 rampIterations = 20;


/*******************************************************************************
* Function Name: Motor_ChangeMotorSpeed
********************************************************************************
* Summary:
*  Ramp the motor speed to a specified percentage.
*
* Parameters:
*  double futureMotorPercentage - Percentage to set the motor too.
*  double secoundsForRamp - Time for the motor's ramping function.
*
* Return:
*  None
*
*******************************************************************************/
void Motor_ChangeMotorSpeed(double futureMotorPercentage, double secoundsForRamp) {
	double currentMotorPercentage = ((double)Motor_PWM_ReadCompare1()) / ((double)Motor_PWM_ReadPeriod());
	double slope = (currentMotorPercentage - futureMotorPercentage) / rampIterations;
	uint32 i, j;
	
	//CYGlobalIntDisable;
	
	for(i=0; i < rampIterations; ++i) {
		uint8 pwmCompareValue = Motor_PWM_ReadPeriod() * (currentMotorPercentage - slope * i);
		Motor_PWM_WriteCompare1(pwmCompareValue);
		
		uint16 delayTime = 10;
		uint32 delays = secoundsForRamp * 1000000 / rampIterations / delayTime;
		for(j=0; j<delays; ++j){
			CyDelayUs(delayTime);
		}
	}
	
	//CyGlobalIntEnable;
}
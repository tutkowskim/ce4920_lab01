/*******************************************************************************
* File Name: Pin_Heart_Test_Point.h  
* Version 2.0
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Pin_Heart_Test_Point_H) /* Pins Pin_Heart_Test_Point_H */
#define CY_PINS_Pin_Heart_Test_Point_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Pin_Heart_Test_Point_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v2_0 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 Pin_Heart_Test_Point__PORT == 15 && ((Pin_Heart_Test_Point__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    Pin_Heart_Test_Point_Write(uint8 value) ;
void    Pin_Heart_Test_Point_SetDriveMode(uint8 mode) ;
uint8   Pin_Heart_Test_Point_ReadDataReg(void) ;
uint8   Pin_Heart_Test_Point_Read(void) ;
uint8   Pin_Heart_Test_Point_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define Pin_Heart_Test_Point_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define Pin_Heart_Test_Point_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define Pin_Heart_Test_Point_DM_RES_UP          PIN_DM_RES_UP
#define Pin_Heart_Test_Point_DM_RES_DWN         PIN_DM_RES_DWN
#define Pin_Heart_Test_Point_DM_OD_LO           PIN_DM_OD_LO
#define Pin_Heart_Test_Point_DM_OD_HI           PIN_DM_OD_HI
#define Pin_Heart_Test_Point_DM_STRONG          PIN_DM_STRONG
#define Pin_Heart_Test_Point_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define Pin_Heart_Test_Point_MASK               Pin_Heart_Test_Point__MASK
#define Pin_Heart_Test_Point_SHIFT              Pin_Heart_Test_Point__SHIFT
#define Pin_Heart_Test_Point_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Pin_Heart_Test_Point_PS                     (* (reg8 *) Pin_Heart_Test_Point__PS)
/* Data Register */
#define Pin_Heart_Test_Point_DR                     (* (reg8 *) Pin_Heart_Test_Point__DR)
/* Port Number */
#define Pin_Heart_Test_Point_PRT_NUM                (* (reg8 *) Pin_Heart_Test_Point__PRT) 
/* Connect to Analog Globals */                                                  
#define Pin_Heart_Test_Point_AG                     (* (reg8 *) Pin_Heart_Test_Point__AG)                       
/* Analog MUX bux enable */
#define Pin_Heart_Test_Point_AMUX                   (* (reg8 *) Pin_Heart_Test_Point__AMUX) 
/* Bidirectional Enable */                                                        
#define Pin_Heart_Test_Point_BIE                    (* (reg8 *) Pin_Heart_Test_Point__BIE)
/* Bit-mask for Aliased Register Access */
#define Pin_Heart_Test_Point_BIT_MASK               (* (reg8 *) Pin_Heart_Test_Point__BIT_MASK)
/* Bypass Enable */
#define Pin_Heart_Test_Point_BYP                    (* (reg8 *) Pin_Heart_Test_Point__BYP)
/* Port wide control signals */                                                   
#define Pin_Heart_Test_Point_CTL                    (* (reg8 *) Pin_Heart_Test_Point__CTL)
/* Drive Modes */
#define Pin_Heart_Test_Point_DM0                    (* (reg8 *) Pin_Heart_Test_Point__DM0) 
#define Pin_Heart_Test_Point_DM1                    (* (reg8 *) Pin_Heart_Test_Point__DM1)
#define Pin_Heart_Test_Point_DM2                    (* (reg8 *) Pin_Heart_Test_Point__DM2) 
/* Input Buffer Disable Override */
#define Pin_Heart_Test_Point_INP_DIS                (* (reg8 *) Pin_Heart_Test_Point__INP_DIS)
/* LCD Common or Segment Drive */
#define Pin_Heart_Test_Point_LCD_COM_SEG            (* (reg8 *) Pin_Heart_Test_Point__LCD_COM_SEG)
/* Enable Segment LCD */
#define Pin_Heart_Test_Point_LCD_EN                 (* (reg8 *) Pin_Heart_Test_Point__LCD_EN)
/* Slew Rate Control */
#define Pin_Heart_Test_Point_SLW                    (* (reg8 *) Pin_Heart_Test_Point__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Pin_Heart_Test_Point_PRTDSI__CAPS_SEL       (* (reg8 *) Pin_Heart_Test_Point__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Pin_Heart_Test_Point_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Pin_Heart_Test_Point__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Pin_Heart_Test_Point_PRTDSI__OE_SEL0        (* (reg8 *) Pin_Heart_Test_Point__PRTDSI__OE_SEL0) 
#define Pin_Heart_Test_Point_PRTDSI__OE_SEL1        (* (reg8 *) Pin_Heart_Test_Point__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Pin_Heart_Test_Point_PRTDSI__OUT_SEL0       (* (reg8 *) Pin_Heart_Test_Point__PRTDSI__OUT_SEL0) 
#define Pin_Heart_Test_Point_PRTDSI__OUT_SEL1       (* (reg8 *) Pin_Heart_Test_Point__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Pin_Heart_Test_Point_PRTDSI__SYNC_OUT       (* (reg8 *) Pin_Heart_Test_Point__PRTDSI__SYNC_OUT) 


#if defined(Pin_Heart_Test_Point__INTSTAT)  /* Interrupt Registers */

    #define Pin_Heart_Test_Point_INTSTAT                (* (reg8 *) Pin_Heart_Test_Point__INTSTAT)
    #define Pin_Heart_Test_Point_SNAP                   (* (reg8 *) Pin_Heart_Test_Point__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_Pin_Heart_Test_Point_H */


/* [] END OF FILE */

/*******************************************************************************
* File Name: ADC_SAR_Heart_INT.c
* Version 2.10
*
*  Description:
*    This file contains the code that operates during the ADC_SAR interrupt
*    service routine.
*
*   Note:
*
********************************************************************************
* Copyright 2008-2013, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "ADC_SAR_Heart.h"


/******************************************************************************
* Custom Declarations and Variables
* - add user inlcude files, prototypes and variables between the following
*   #START and #END tags
******************************************************************************/
/* `#START ADC_SYS_VAR`  */
#include <device.h>
extern uint16 heartRateSensor;
/* `#END`  */


#if(ADC_SAR_Heart_IRQ_REMOVE == 0u)


    /******************************************************************************
    * Function Name: ADC_SAR_Heart_ISR
    *******************************************************************************
    *
    * Summary:
    *  Handle Interrupt Service Routine.
    *
    * Parameters:
    *  None.
    *
    * Return:
    *  None.
    *
    * Reentrant:
    *  No.
    *
    ******************************************************************************/
    CY_ISR( ADC_SAR_Heart_ISR )
    {
        /************************************************************************
        *  Custom Code
        *  - add user ISR code between the following #START and #END tags
        *************************************************************************/
          /* `#START MAIN_ADC_ISR`  */
		uint8 adcResult = ADC_SAR_Heart_GetResult8();
		heartRateSensor = ADC_SAR_Heart_CountsTo_mVolts(adcResult);
		VDAC8_1_SetValue(adcResult / 2);
		Timer_2_WriteCounter(Timer_2_ReadPeriod());
          /* `#END`  */

    }

#endif   /* End ADC_SAR_Heart_IRQ_REMOVE */

/* [] END OF FILE */

/*******************************************************************************
* File Name: Timer_Heart_PM.c
* Version 2.50
*
*  Description:
*     This file provides the power management source code to API for the
*     Timer.
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
********************************************************************************/

#include "Timer_Heart.h"
static Timer_Heart_backupStruct Timer_Heart_backup;


/*******************************************************************************
* Function Name: Timer_Heart_SaveConfig
********************************************************************************
*
* Summary:
*     Save the current user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  Timer_Heart_backup:  Variables of this global structure are modified to
*  store the values of non retention configuration registers when Sleep() API is
*  called.
*
*******************************************************************************/
void Timer_Heart_SaveConfig(void) 
{
    #if (!Timer_Heart_UsingFixedFunction)
        /* Backup the UDB non-rentention registers for CY_UDB_V0 */
        #if (CY_UDB_V0)
            Timer_Heart_backup.TimerUdb = Timer_Heart_ReadCounter();
            Timer_Heart_backup.TimerPeriod = Timer_Heart_ReadPeriod();
            Timer_Heart_backup.InterruptMaskValue = Timer_Heart_STATUS_MASK;
            #if (Timer_Heart_UsingHWCaptureCounter)
                Timer_Heart_backup.TimerCaptureCounter = Timer_Heart_ReadCaptureCount();
            #endif /* Backup the UDB non-rentention register capture counter for CY_UDB_V0 */
        #endif /* Backup the UDB non-rentention registers for CY_UDB_V0 */

        #if (CY_UDB_V1)
            Timer_Heart_backup.TimerUdb = Timer_Heart_ReadCounter();
            Timer_Heart_backup.InterruptMaskValue = Timer_Heart_STATUS_MASK;
            #if (Timer_Heart_UsingHWCaptureCounter)
                Timer_Heart_backup.TimerCaptureCounter = Timer_Heart_ReadCaptureCount();
            #endif /* Back Up capture counter register  */
        #endif /* Backup non retention registers, interrupt mask and capture counter for CY_UDB_V1 */

        #if(!Timer_Heart_ControlRegRemoved)
            Timer_Heart_backup.TimerControlRegister = Timer_Heart_ReadControlRegister();
        #endif /* Backup the enable state of the Timer component */
    #endif /* Backup non retention registers in UDB implementation. All fixed function registers are retention */
}


/*******************************************************************************
* Function Name: Timer_Heart_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  Timer_Heart_backup:  Variables of this global structure are used to
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void Timer_Heart_RestoreConfig(void) 
{   
    #if (!Timer_Heart_UsingFixedFunction)
        /* Restore the UDB non-rentention registers for CY_UDB_V0 */
        #if (CY_UDB_V0)
            /* Interrupt State Backup for Critical Region*/
            uint8 Timer_Heart_interruptState;

            Timer_Heart_WriteCounter(Timer_Heart_backup.TimerUdb);
            Timer_Heart_WritePeriod(Timer_Heart_backup.TimerPeriod);
            /* CyEnterCriticalRegion and CyExitCriticalRegion are used to mark following region critical*/
            /* Enter Critical Region*/
            Timer_Heart_interruptState = CyEnterCriticalSection();
            /* Use the interrupt output of the status register for IRQ output */
            Timer_Heart_STATUS_AUX_CTRL |= Timer_Heart_STATUS_ACTL_INT_EN_MASK;
            /* Exit Critical Region*/
            CyExitCriticalSection(Timer_Heart_interruptState);
            Timer_Heart_STATUS_MASK =Timer_Heart_backup.InterruptMaskValue;
            #if (Timer_Heart_UsingHWCaptureCounter)
                Timer_Heart_SetCaptureCount(Timer_Heart_backup.TimerCaptureCounter);
            #endif /* Restore the UDB non-rentention register capture counter for CY_UDB_V0 */
        #endif /* Restore the UDB non-rentention registers for CY_UDB_V0 */

        #if (CY_UDB_V1)
            Timer_Heart_WriteCounter(Timer_Heart_backup.TimerUdb);
            Timer_Heart_STATUS_MASK =Timer_Heart_backup.InterruptMaskValue;
            #if (Timer_Heart_UsingHWCaptureCounter)
                Timer_Heart_SetCaptureCount(Timer_Heart_backup.TimerCaptureCounter);
            #endif /* Restore Capture counter register*/
        #endif /* Restore up non retention registers, interrupt mask and capture counter for CY_UDB_V1 */

        #if(!Timer_Heart_ControlRegRemoved)
            Timer_Heart_WriteControlRegister(Timer_Heart_backup.TimerControlRegister);
        #endif /* Restore the enable state of the Timer component */
    #endif /* Restore non retention registers in the UDB implementation only */
}


/*******************************************************************************
* Function Name: Timer_Heart_Sleep
********************************************************************************
*
* Summary:
*     Stop and Save the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  Timer_Heart_backup.TimerEnableState:  Is modified depending on the
*  enable state of the block before entering sleep mode.
*
*******************************************************************************/
void Timer_Heart_Sleep(void) 
{
    #if(!Timer_Heart_ControlRegRemoved)
        /* Save Counter's enable state */
        if(Timer_Heart_CTRL_ENABLE == (Timer_Heart_CONTROL & Timer_Heart_CTRL_ENABLE))
        {
            /* Timer is enabled */
            Timer_Heart_backup.TimerEnableState = 1u;
        }
        else
        {
            /* Timer is disabled */
            Timer_Heart_backup.TimerEnableState = 0u;
        }
    #endif /* Back up enable state from the Timer control register */
    Timer_Heart_Stop();
    Timer_Heart_SaveConfig();
}


/*******************************************************************************
* Function Name: Timer_Heart_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  Timer_Heart_backup.enableState:  Is used to restore the enable state of
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void Timer_Heart_Wakeup(void) 
{
    Timer_Heart_RestoreConfig();
    #if(!Timer_Heart_ControlRegRemoved)
        if(Timer_Heart_backup.TimerEnableState == 1u)
        {     /* Enable Timer's operation */
                Timer_Heart_Enable();
        } /* Do nothing if Timer was disabled before */
    #endif /* Remove this code section if Control register is removed */
}


/* [] END OF FILE */

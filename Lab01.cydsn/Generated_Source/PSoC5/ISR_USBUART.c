/*******************************************************************************
* File Name: ISR_USBUART.c  
* Version 1.70
*
*  Description:
*   API for controlling the state of an interrupt.
*
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/


#include <cydevice_trm.h>
#include <CyLib.h>
#include <ISR_USBUART.h>

#if !defined(ISR_USBUART__REMOVED) /* Check for removal by optimization */

/*******************************************************************************
*  Place your includes, defines and code here 
********************************************************************************/
/* `#START ISR_USBUART_intc` */
#include <device.h>
extern uint8 echoStatus;
/* `#END` */

#ifndef CYINT_IRQ_BASE
#define CYINT_IRQ_BASE      16
#endif /* CYINT_IRQ_BASE */
#ifndef CYINT_VECT_TABLE
#define CYINT_VECT_TABLE    ((cyisraddress **) CYREG_NVIC_VECT_OFFSET)
#endif /* CYINT_VECT_TABLE */

/* Declared in startup, used to set unused interrupts to. */
CY_ISR_PROTO(IntDefaultHandler);


/*******************************************************************************
* Function Name: ISR_USBUART_Start
********************************************************************************
*
* Summary:
*  Set up the interrupt and enable it.
*
* Parameters:  
*   None
*
* Return:
*   None
*
*******************************************************************************/
void ISR_USBUART_Start(void)
{
    /* For all we know the interrupt is active. */
    ISR_USBUART_Disable();

    /* Set the ISR to point to the ISR_USBUART Interrupt. */
    ISR_USBUART_SetVector(&ISR_USBUART_Interrupt);

    /* Set the priority. */
    ISR_USBUART_SetPriority((uint8)ISR_USBUART_INTC_PRIOR_NUMBER);

    /* Enable it. */
    ISR_USBUART_Enable();
}


/*******************************************************************************
* Function Name: ISR_USBUART_StartEx
********************************************************************************
*
* Summary:
*  Set up the interrupt and enable it.
*
* Parameters:  
*   address: Address of the ISR to set in the interrupt vector table.
*
* Return:
*   None
*
*******************************************************************************/
void ISR_USBUART_StartEx(cyisraddress address)
{
    /* For all we know the interrupt is active. */
    ISR_USBUART_Disable();

    /* Set the ISR to point to the ISR_USBUART Interrupt. */
    ISR_USBUART_SetVector(address);

    /* Set the priority. */
    ISR_USBUART_SetPriority((uint8)ISR_USBUART_INTC_PRIOR_NUMBER);

    /* Enable it. */
    ISR_USBUART_Enable();
}


/*******************************************************************************
* Function Name: ISR_USBUART_Stop
********************************************************************************
*
* Summary:
*   Disables and removes the interrupt.
*
* Parameters:  
*
* Return:
*   None
*
*******************************************************************************/
void ISR_USBUART_Stop(void)
{
    /* Disable this interrupt. */
    ISR_USBUART_Disable();

    /* Set the ISR to point to the passive one. */
    ISR_USBUART_SetVector(&IntDefaultHandler);
}


/*******************************************************************************
* Function Name: ISR_USBUART_Interrupt
********************************************************************************
*
* Summary:
*   The default Interrupt Service Routine for ISR_USBUART.
*
*   Add custom code between the coments to keep the next version of this file
*   from over writting your code.
*
* Parameters:  
*
* Return:
*   None
*
*******************************************************************************/
CY_ISR(ISR_USBUART_Interrupt)
{
    /*  Place your Interrupt code here. */
    /* `#START ISR_USBUART_Interrupt` */
	echoStatus = 1;
	Timer_1_WriteCounter(Timer_1_ReadPeriod());
	
    /* `#END` */
}


/*******************************************************************************
* Function Name: ISR_USBUART_SetVector
********************************************************************************
*
* Summary:
*   Change the ISR vector for the Interrupt. Note calling ISR_USBUART_Start
*   will override any effect this method would have had. To set the vector 
*   before the component has been started use ISR_USBUART_StartEx instead.
*
* Parameters:
*   address: Address of the ISR to set in the interrupt vector table.
*
* Return:
*   None
*
*******************************************************************************/
void ISR_USBUART_SetVector(cyisraddress address)
{
    cyisraddress * ramVectorTable;

    ramVectorTable = (cyisraddress *) *CYINT_VECT_TABLE;

    ramVectorTable[CYINT_IRQ_BASE + (uint32)ISR_USBUART__INTC_NUMBER] = address;
}


/*******************************************************************************
* Function Name: ISR_USBUART_GetVector
********************************************************************************
*
* Summary:
*   Gets the "address" of the current ISR vector for the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   Address of the ISR in the interrupt vector table.
*
*******************************************************************************/
cyisraddress ISR_USBUART_GetVector(void)
{
    cyisraddress * ramVectorTable;

    ramVectorTable = (cyisraddress *) *CYINT_VECT_TABLE;

    return ramVectorTable[CYINT_IRQ_BASE + (uint32)ISR_USBUART__INTC_NUMBER];
}


/*******************************************************************************
* Function Name: ISR_USBUART_SetPriority
********************************************************************************
*
* Summary:
*   Sets the Priority of the Interrupt. Note calling ISR_USBUART_Start
*   or ISR_USBUART_StartEx will override any effect this method 
*   would have had. This method should only be called after 
*   ISR_USBUART_Start or ISR_USBUART_StartEx has been called. To set 
*   the initial priority for the component use the cydwr file in the tool.
*
* Parameters:
*   priority: Priority of the interrupt. 0 - 7, 0 being the highest.
*
* Return:
*   None
*
*******************************************************************************/
void ISR_USBUART_SetPriority(uint8 priority)
{
    *ISR_USBUART_INTC_PRIOR = priority << 5;
}


/*******************************************************************************
* Function Name: ISR_USBUART_GetPriority
********************************************************************************
*
* Summary:
*   Gets the Priority of the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   Priority of the interrupt. 0 - 7, 0 being the highest.
*
*******************************************************************************/
uint8 ISR_USBUART_GetPriority(void)
{
    uint8 priority;


    priority = *ISR_USBUART_INTC_PRIOR >> 5;

    return priority;
}


/*******************************************************************************
* Function Name: ISR_USBUART_Enable
********************************************************************************
*
* Summary:
*   Enables the interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void ISR_USBUART_Enable(void)
{
    /* Enable the general interrupt. */
    *ISR_USBUART_INTC_SET_EN = ISR_USBUART__INTC_MASK;
}


/*******************************************************************************
* Function Name: ISR_USBUART_GetState
********************************************************************************
*
* Summary:
*   Gets the state (enabled, disabled) of the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   1 if enabled, 0 if disabled.
*
*******************************************************************************/
uint8 ISR_USBUART_GetState(void)
{
    /* Get the state of the general interrupt. */
    return ((*ISR_USBUART_INTC_SET_EN & (uint32)ISR_USBUART__INTC_MASK) != 0u) ? 1u:0u;
}


/*******************************************************************************
* Function Name: ISR_USBUART_Disable
********************************************************************************
*
* Summary:
*   Disables the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void ISR_USBUART_Disable(void)
{
    /* Disable the general interrupt. */
    *ISR_USBUART_INTC_CLR_EN = ISR_USBUART__INTC_MASK;
}


/*******************************************************************************
* Function Name: ISR_USBUART_SetPending
********************************************************************************
*
* Summary:
*   Causes the Interrupt to enter the pending state, a software method of
*   generating the interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void ISR_USBUART_SetPending(void)
{
    *ISR_USBUART_INTC_SET_PD = ISR_USBUART__INTC_MASK;
}


/*******************************************************************************
* Function Name: ISR_USBUART_ClearPending
********************************************************************************
*
* Summary:
*   Clears a pending interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void ISR_USBUART_ClearPending(void)
{
    *ISR_USBUART_INTC_CLR_PD = ISR_USBUART__INTC_MASK;
}

#endif /* End check for removal by optimization */


/* [] END OF FILE */

#include <device.h>
#include "my_uart.h"


/*******************************************************************************
* Function Name: USBUART_PRINT_LINE
********************************************************************************
* Summary:
*  Print the null terminated line over the USB uart if attached
*
* Parameters:
*
* Return:
*
*******************************************************************************/
void USBUART_PRINT_LINE(char * line) {
	static uint8 uart_needs_init = 1;
	
	if(!USBUART_GetConfiguration()) {
		uart_needs_init = 1;
		return;
	}
	
	if(uart_needs_init) {
		uart_needs_init = 0;
		USBUART_CDC_Init();
		return;
	}
	
	USBUART_PutString(line);
	while(!USBUART_CDCIsReady());
	USBUART_PutCRLF();
	while(!USBUART_CDCIsReady());
}


/*******************************************************************************
* Function Name: USBUART_PRINT_LINE
********************************************************************************
* Summary:
*  Print the null terminated line over the USB uart if attached
*
* Parameters:
*
* Return:
*
*******************************************************************************/
void USBUART_PRINT_STRING(char * line) {
	static uint8 uart_needs_init = 1;
	
	if(!USBUART_GetConfiguration()) {
		uart_needs_init = 1;
		return;
	}
	
	if(uart_needs_init) {
		uart_needs_init = 0;
		USBUART_CDC_Init();
		return;
	}
	
	USBUART_PutString(line);
}
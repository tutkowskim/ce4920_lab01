#include <device.h>
#include "my_lcd.h"


/*******************************************************************************
* Function Name: LCD_Print_Welcome_Message
********************************************************************************
* Summary:
*  Print a Welcome Message onto the Screen
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void LCD_Print_Welcome_Message() {
	LCD_ClearDisplay();
    LCD_Position(0, 3);
    LCD_PrintString("Welcome to");
    LCD_Position(1, 2);
    LCD_PrintString("My Treadmill");
}


/*******************************************************************************
* Function Name: Display_Status_Bar
********************************************************************************
* Summary:
*  Display a status bar accross the LCD
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void Display_Status_Bar() {
	uint8 custom_chars[] = {
		LCD_CUSTOM_0,
		LCD_CUSTOM_1,
		LCD_CUSTOM_2,
		LCD_CUSTOM_3,
		LCD_CUSTOM_4,
		LCD_CUSTOM_5};
	
	uint8 sizeof_custom_chars = sizeof(custom_chars)/sizeof(custom_chars[0]);
	
	uint8 i, j;
	for(i = 0; i < 16; ++i) {
		for(j = 0; j < sizeof_custom_chars; ++j) {
			LCD_Position(0, i);
			LCD_PutChar(custom_chars[j]);
			CyDelay(100/sizeof_custom_chars);
		}
	}
}
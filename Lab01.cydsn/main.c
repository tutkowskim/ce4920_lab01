#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <device.h>

#include "profile.h"
#include "my_speaker.h"
#include "my_lcd.h"
#include "my_capsense.h"
#include "my_uart.h"
#include "my_motor.h"

#define true 		(1)
#define false 		(0)	


enum states_t {
    RESET, 
	WAIT,
	PROFILE_SELECTION,
    RAMPUP, 
    MONITOR, 
    STOP
};

char outputBuffer[150];

uint8 echoStatus = 0;
uint16 heartRate = 1;
uint16 averageHeartRate;
int16 heartRateSensor = 1;
uint8 selectedProfileIndex = 0;
uint8 numberOfUserProfiles = 5;
struct userProfile_t userProfiles[5];
enum states_t state = RESET;


/*******************************************************************************
* Function Name: generateUserProfiles
********************************************************************************
* Summary:
*  Generate the list of user profiles
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void generateUserProfiles() {
	userProfiles[0].profileDisplayName = "Zombie     ";
	userProfiles[0].motorPercentage = 0.10;
	userProfiles[0].criticalMotorPercentage = 0.05;
	
	userProfiles[1].profileDisplayName = "Programmer ";
	userProfiles[1].motorPercentage = 0.25;
	userProfiles[1].criticalMotorPercentage = 0.15;
	
	userProfiles[2].profileDisplayName = "Plumber Joe";
	userProfiles[2].motorPercentage = 0.50;
	userProfiles[2].criticalMotorPercentage = 0.30;
	
	userProfiles[3].profileDisplayName = "Athlete    ";
	userProfiles[3].motorPercentage = 0.75;
	userProfiles[3].criticalMotorPercentage = 0.60;
	
	userProfiles[4].profileDisplayName = "Jaguar     ";
	userProfiles[4].motorPercentage = 1.00;
	userProfiles[4].criticalMotorPercentage = 0.75;
}


/*******************************************************************************
* Function Name: main
********************************************************************************
* Summary:
*  Program entry point and finite state machine control loop.
*
* Return:
*  int - program exit code, which should never be returned
*
*******************************************************************************/
int main() {
	double motorPercentage;
	uint32 debounce;
	sprintf(outputBuffer, "Empty Buffer %i.\n\r", 0);
	
    while(true) {		
        switch(state) {
            case RESET:
				debounce = 0;
			 	motorPercentage = 0;
				selectedProfileIndex = 0;
				generateUserProfiles();
				
                // Enable Interupts
                CyGlobalIntEnable;
				
				// Setup Heart Rate Monitor
				Timer_Heart_Start();
				Comp_Heart_Start();
				Timer_2_Start();
				ADC_SAR_Heart_Start();
				ADC_SAR_Heart_Enable();
				ADC_SAR_Heart_IRQ_Enable();
				VDAC8_1_Start();
				ISR_HEART_BEAT_Start();
				
				// Setup Motor
				Motor_PWM_Start();
				Motor_PWM_WriteCompare1(0x00);
				Motor_PWM_Sleep();
				
                // Init Cap Sensor
                CapSense_Start();
                CapSense_InitializeAllBaselines();
                CapSense_ScanEnabledWidgets();
				
                // Init LCD
                LCD_Init();
                    
				// Display Status Bar
				Display_Status_Bar();
				
                // Welcome User
                LCD_Print_Welcome_Message();
                Speaker_Play_Welcome_Music();
				
				// Setup USBUART
				USBUART_Start(0u, USBUART_3V_OPERATION);
				Timer_1_Start();
				ISR_USBUART_Start();
				
                state = WAIT;
                break;
                
            case WAIT:
				LCD_ClearDisplay();
                if(Cap_Sensor_Swipe_Detected() == RIGHT_SWIPE) {
					state = PROFILE_SELECTION;
                }
                break;
			
			case PROFILE_SELECTION:
				if(Cap_Sensor_Button_Status(CapSense_BUTTON0__BTN) == ON && !debounce) {
					debounce = 1;
					selectedProfileIndex = selectedProfileIndex < sizeof(userProfiles)/sizeof(userProfiles[0])-1 ? selectedProfileIndex+1 : 0;
				} else if(Cap_Sensor_Button_Status(CapSense_BUTTON0__BTN) == OFF){
					debounce = 0;
				}
				
				LCD_Position(0, 0);
				LCD_PrintString("Select Profile:");
				LCD_Position(1, 0);
				LCD_PrintString(userProfiles[selectedProfileIndex].profileDisplayName);
				
				if(Cap_Sensor_Button_Status(CapSense_BUTTON1__BTN) == ON) {
					uint8 resetProfileNameIndex;
					for(resetProfileNameIndex = 0; resetProfileNameIndex < 4; ++resetProfileNameIndex) {
						LCD_Position(1, 0);
						LCD_PrintString("               ");
						CyDelay(250);
						LCD_Position(1, 0);
						LCD_PrintString(userProfiles[selectedProfileIndex].profileDisplayName);
						CyDelay(250);
						
						state = RAMPUP;
					}
				}
				break;
			
            case RAMPUP:
				LCD_ClearDisplay();
				LCD_Position(0, 0);
				LCD_PrintString("    RAMP UP");
				motorPercentage = userProfiles[selectedProfileIndex].motorPercentage;
				
				Motor_PWM_Wakeup();
				Motor_ChangeMotorSpeed(motorPercentage, 8.0);
				
				state = MONITOR;
                break;
                
            case MONITOR:
				LCD_Position(0, 0);
				LCD_PrintString("    Monitor");
				LCD_Position(1, 0);
				LCD_PrintString("H:        M:      ");
				LCD_Position(1, 2);
				LCD_PrintNumber(heartRate);
				LCD_Position(1, 12);
				LCD_PrintNumber((int)(motorPercentage * 100));
				LCD_PutChar('%');
				
				uint8 critcalModeEnabled = heartRate >= (averageHeartRate + averageHeartRate * 0.10);
				
				if(Cap_Sensor_Button_Status(CapSense_BUTTON0__BTN) == ON) {
					state = STOP;
				} else if (critcalModeEnabled && motorPercentage != userProfiles[selectedProfileIndex].criticalMotorPercentage) {
					motorPercentage = userProfiles[selectedProfileIndex].criticalMotorPercentage;
					Motor_ChangeMotorSpeed(motorPercentage, 2.0);
				} else if (!critcalModeEnabled && motorPercentage != userProfiles[selectedProfileIndex].motorPercentage) {
					motorPercentage = userProfiles[selectedProfileIndex].motorPercentage;
					Motor_ChangeMotorSpeed(motorPercentage, 2.0);
				}
                break;
                
            case STOP:
				LCD_ClearDisplay();
				LCD_Position(0, 0);
				LCD_PrintString("    Stoping");
				Motor_ChangeMotorSpeed(0.0, 2.0);
				Motor_PWM_Sleep();
				state = WAIT;
                break;
                
            default:
				LCD_ClearDisplay();
				LCD_Position(0, 0);
				LCD_PrintString("  Unkown State!");
				LCD_Position(1, 0);
				LCD_PrintString("  Please Reset");
				while(1){}
                break;
        }
		
		if(state == MONITOR && echoStatus && USBUART_CDCIsReady()) {
			sprintf(
				outputBuffer, 
				"Profile:%s\r\nMotorspeed:%i%%\r\nHeartrateSensor:%i\r\nHeartrate:%i\r\nAverageHeartrate:%i\r\n", 
				userProfiles[selectedProfileIndex].profileDisplayName,
				(int)(motorPercentage * 100),
				(int)heartRateSensor,
				(int)(heartRate),
				(int)(averageHeartRate));
			USBUART_PRINT_STRING(outputBuffer);
			echoStatus = 0;
		}
    }
}

#ifndef MY_MOTOR_H
#define MY_MOTOR_H
#include <device.h>
	
void Motor_ChangeMotorSpeed(double futureMotorPercentage, double secoundsForRamp);

#endif
#include <device.h>
#include "my_capsense.h"


/*******************************************************************************
* Function Name: Cap_Sensor_Swipe_Detected
********************************************************************************
* Summary:
*  function determines if a swipe has occured on the cap slider
*
* Parameters:
*  None
*
* Return:
*  uinit swipeDirection - possible values include:
*   - LEFT_SWIPE
*   - RIGHT_SWIPE
*   - NO_SWIPE
*
*******************************************************************************/
int Cap_Sensor_Swipe_Detected() {
	int swipe = NO_SWIPE;
	
  	if(!CapSense_IsBusy()) {
		CapSense_UpdateEnabledBaselines();
	    CapSense_ScanEnabledWidgets();
	    while(CapSense_IsBusy());
		
	    static uint16 previousPosition = NO_FINGER;
	    static uint16 startPosition = NO_FINGER;
		
	    uint16 currentPosition = (uint8)CapSense_GetCentroidPos(CapSense_LINEARSLIDER0__LS);

	    if(previousPosition == NO_FINGER && currentPosition != NO_FINGER) {
	        startPosition = currentPosition;
	    } else if(previousPosition != NO_FINGER && currentPosition == NO_FINGER){
	        if(startPosition < previousPosition) {
	            swipe = RIGHT_SWIPE;
	        } else if (startPosition > previousPosition) {
	            swipe = LEFT_SWIPE;
	        }
	    }
		previousPosition = currentPosition;
	}
    return swipe;
}


/*******************************************************************************
* Function Name: Cap_Sensor_Button_Status
********************************************************************************
* Summary:
*  function determines the status of a button
*
* Parameters:
*  uint8 button - Cap button to test. Possible values:
*    - CapSense_BUTTON0__BTN
*    - CapSense_BUTTON1__BTN
*
* Return:
*  uinit status - Possible Values:
*    -- ON (1)
*    -- OFF (0)
*
*******************************************************************************/
int Cap_Sensor_Button_Status(uint8 button) {	
	static int retVal = OFF;
	
	if(CapSense_IsBusy()){
		return retVal;
	}
	
	CapSense_UpdateEnabledBaselines();
    CapSense_ScanEnabledWidgets();
	
	while(CapSense_IsBusy());
	return retVal = CapSense_CheckIsWidgetActive(button);;
}


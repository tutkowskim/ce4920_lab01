#ifndef PROFILE_H
#define PROFILE_H

struct userProfile_t {
	char * profileDisplayName;
	float motorPercentage;			// The motor speed as a percentage of the max speed
	float criticalMotorPercentage;	// The motor speed as a percentage of the max speed
};

#endif
#ifndef MY_SPEAKER_H
#define MY_SPEAKER_H

void Speaker_Play_Tone(double freq, double length);
void Speaker_Play_Welcome_Music();

#endif
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace PCAPP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            this.serialPort = null;
            InitializeComponent();

            //TODO: Properly handle multiple threads
            Chart.CheckForIllegalCrossThreadCalls = false;
            Label.CheckForIllegalCrossThreadCalls = false;

            this.comboBoxComPort.Items.AddRange(SerialPort.GetPortNames());
            this.chart.Series["Series1"].ChartType = SeriesChartType.Line;
            this.chart.Series["Series1"].Color = System.Drawing.Color.DarkRed;
            this.buttonDisconnectClick(null, null);
        }

        private void chart1_Click(object sender, EventArgs e) {/* Intentionally left blank */}

        private void buttonConnectClick(object sender, EventArgs e)
        {
            try
            {
                this.serialPort = new SerialPort(comboBoxComPort.SelectedItem.ToString());
                this.serialPort.DataReceived += this.dataReceived;
                this.serialPort.Open();
                this.labelConnectedValue.Text = (serialPort != null).ToString();
            } 
            catch (Exception ex) 
            {
                MessageBox.Show(this, "Invalid comport.");
            }
        }

        private void buttonDisconnectClick(object sender, EventArgs e)
        {
            if(this.serialPort != null)
            {
                this.serialPort.Close();
            }

            this.serialPort = null;
            this.labelConnectedValue.Text = (serialPort != null).ToString();
            this.labelProfileValue.Text = "";
            this.labelHeartRateValue.Text = "";
            this.labelMotorSpeedValue.Text = "";
        }

        private void dataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            String line = this.serialPort.ReadLine();
            String catagory;
            String value;

            try
            {
                catagory = line.Split(':')[0];
                value = line.Split(':')[1];
            }
            catch(Exception ex)
            {
                return;
            }

            if(catagory.Equals("Profile")) 
            {
                this.labelProfileValue.Text = value;
            }
            else if(catagory.Equals("Heartrate")) 
            {
                this.labelHeartRateValue.Text = value;
            }
            else if (catagory.Equals("HeartrateSensor"))
            {
                try
                {
                    this.chart.Series["Series1"].Points.AddY(Convert.ToDouble(value));
                } 
                catch(Exception ex) 
                {
                    return;
                }
                if (this.chart.Series["Series1"].Points.Count > 10)
                {
                    this.chart.Series["Series1"].Points.RemoveAt(0);
                }
            }
            else if (catagory.Equals("Motorspeed"))
            {
                this.labelMotorSpeedValue.Text = value;
            }
        }
    }
}

﻿namespace PCAPP
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.labelCommunications = new System.Windows.Forms.Label();
            this.comboBoxComPort = new System.Windows.Forms.ComboBox();
            this.labelComPort = new System.Windows.Forms.Label();
            this.buttonDisconnect = new System.Windows.Forms.Button();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.labelStatus = new System.Windows.Forms.Label();
            this.labelConnnected = new System.Windows.Forms.Label();
            this.labelConnectedValue = new System.Windows.Forms.Label();
            this.labelProfileValue = new System.Windows.Forms.Label();
            this.labelProfile = new System.Windows.Forms.Label();
            this.labelHeartRateValue = new System.Windows.Forms.Label();
            this.labelHeartRate = new System.Windows.Forms.Label();
            this.labelMotorSpeedValue = new System.Windows.Forms.Label();
            this.labelMotorSpeed = new System.Windows.Forms.Label();
            this.serialPort = new System.IO.Ports.SerialPort(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
            this.SuspendLayout();
            // 
            // chart
            // 
            this.chart.BackHatchStyle = System.Windows.Forms.DataVisualization.Charting.ChartHatchStyle.BackwardDiagonal;
            chartArea4.Name = "ChartArea1";
            this.chart.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.chart.Legends.Add(legend4);
            this.chart.Location = new System.Drawing.Point(12, 12);
            this.chart.Name = "chart";
            this.chart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.chart.Series.Add(series4);
            this.chart.Size = new System.Drawing.Size(300, 300);
            this.chart.TabIndex = 0;
            this.chart.Text = "chart1";
            // 
            // labelCommunications
            // 
            this.labelCommunications.AutoSize = true;
            this.labelCommunications.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCommunications.Location = new System.Drawing.Point(318, 12);
            this.labelCommunications.Name = "labelCommunications";
            this.labelCommunications.Size = new System.Drawing.Size(125, 17);
            this.labelCommunications.TabIndex = 1;
            this.labelCommunications.Text = "Communications";
            // 
            // comboBoxComPort
            // 
            this.comboBoxComPort.FormattingEnabled = true;
            this.comboBoxComPort.Location = new System.Drawing.Point(391, 34);
            this.comboBoxComPort.Name = "comboBoxComPort";
            this.comboBoxComPort.Size = new System.Drawing.Size(121, 24);
            this.comboBoxComPort.TabIndex = 2;
            // 
            // labelComPort
            // 
            this.labelComPort.AutoSize = true;
            this.labelComPort.Location = new System.Drawing.Point(319, 37);
            this.labelComPort.Name = "labelComPort";
            this.labelComPort.Size = new System.Drawing.Size(70, 17);
            this.labelComPort.TabIndex = 3;
            this.labelComPort.Text = "Com Port:";
            // 
            // buttonDisconnect
            // 
            this.buttonDisconnect.Location = new System.Drawing.Point(419, 64);
            this.buttonDisconnect.Name = "buttonDisconnect";
            this.buttonDisconnect.Size = new System.Drawing.Size(93, 23);
            this.buttonDisconnect.TabIndex = 4;
            this.buttonDisconnect.Text = "Disconnect";
            this.buttonDisconnect.UseVisualStyleBackColor = true;
            this.buttonDisconnect.Click += new System.EventHandler(this.buttonDisconnectClick);
            // 
            // buttonConnect
            // 
            this.buttonConnect.Location = new System.Drawing.Point(322, 64);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(91, 23);
            this.buttonConnect.TabIndex = 5;
            this.buttonConnect.Text = "Connect";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnectClick);
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatus.Location = new System.Drawing.Point(322, 94);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(54, 17);
            this.labelStatus.TabIndex = 6;
            this.labelStatus.Text = "Status";
            // 
            // labelConnnected
            // 
            this.labelConnnected.AutoSize = true;
            this.labelConnnected.Location = new System.Drawing.Point(322, 120);
            this.labelConnnected.Name = "labelConnnected";
            this.labelConnnected.Size = new System.Drawing.Size(80, 17);
            this.labelConnnected.TabIndex = 7;
            this.labelConnnected.Text = "Connected:";
            // 
            // labelConnectedValue
            // 
            this.labelConnectedValue.AutoSize = true;
            this.labelConnectedValue.Location = new System.Drawing.Point(408, 120);
            this.labelConnectedValue.Name = "labelConnectedValue";
            this.labelConnectedValue.Size = new System.Drawing.Size(0, 17);
            this.labelConnectedValue.TabIndex = 8;
            // 
            // labelProfileValue
            // 
            this.labelProfileValue.AutoSize = true;
            this.labelProfileValue.Location = new System.Drawing.Point(408, 146);
            this.labelProfileValue.Name = "labelProfileValue";
            this.labelProfileValue.Size = new System.Drawing.Size(0, 17);
            this.labelProfileValue.TabIndex = 10;
            // 
            // labelProfile
            // 
            this.labelProfile.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.labelProfile.AutoSize = true;
            this.labelProfile.Location = new System.Drawing.Point(322, 146);
            this.labelProfile.Name = "labelProfile";
            this.labelProfile.Size = new System.Drawing.Size(49, 17);
            this.labelProfile.TabIndex = 9;
            this.labelProfile.Text = "Profle:";
            // 
            // labelHeartRateValue
            // 
            this.labelHeartRateValue.AutoSize = true;
            this.labelHeartRateValue.Location = new System.Drawing.Point(408, 172);
            this.labelHeartRateValue.Name = "labelHeartRateValue";
            this.labelHeartRateValue.Size = new System.Drawing.Size(0, 17);
            this.labelHeartRateValue.TabIndex = 12;
            // 
            // labelHeartRate
            // 
            this.labelHeartRate.AutoSize = true;
            this.labelHeartRate.Location = new System.Drawing.Point(322, 172);
            this.labelHeartRate.Name = "labelHeartRate";
            this.labelHeartRate.Size = new System.Drawing.Size(81, 17);
            this.labelHeartRate.TabIndex = 11;
            this.labelHeartRate.Text = "Heart Rate:";
            // 
            // labelMotorSpeedValue
            // 
            this.labelMotorSpeedValue.AutoSize = true;
            this.labelMotorSpeedValue.Location = new System.Drawing.Point(408, 199);
            this.labelMotorSpeedValue.Name = "labelMotorSpeedValue";
            this.labelMotorSpeedValue.Size = new System.Drawing.Size(0, 17);
            this.labelMotorSpeedValue.TabIndex = 14;
            // 
            // labelMotorSpeed
            // 
            this.labelMotorSpeed.AutoSize = true;
            this.labelMotorSpeed.Location = new System.Drawing.Point(322, 199);
            this.labelMotorSpeed.Name = "labelMotorSpeed";
            this.labelMotorSpeed.Size = new System.Drawing.Size(93, 17);
            this.labelMotorSpeed.TabIndex = 13;
            this.labelMotorSpeed.Text = "Motor Speed:";
            // 
            // serialPort
            // 
            this.serialPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.dataReceived);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 321);
            this.Controls.Add(this.labelMotorSpeedValue);
            this.Controls.Add(this.labelMotorSpeed);
            this.Controls.Add(this.labelHeartRateValue);
            this.Controls.Add(this.labelHeartRate);
            this.Controls.Add(this.labelProfileValue);
            this.Controls.Add(this.labelProfile);
            this.Controls.Add(this.labelConnectedValue);
            this.Controls.Add(this.labelConnnected);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.buttonConnect);
            this.Controls.Add(this.buttonDisconnect);
            this.Controls.Add(this.labelComPort);
            this.Controls.Add(this.comboBoxComPort);
            this.Controls.Add(this.labelCommunications);
            this.Controls.Add(this.chart);
            this.Name = "Form1";
            this.Text = "My Treadmill";
            ((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart;
        private System.Windows.Forms.Label labelCommunications;
        private System.Windows.Forms.ComboBox comboBoxComPort;
        private System.Windows.Forms.Label labelComPort;
        private System.Windows.Forms.Button buttonDisconnect;
        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Label labelConnnected;
        private System.Windows.Forms.Label labelConnectedValue;
        private System.Windows.Forms.Label labelProfileValue;
        private System.Windows.Forms.Label labelProfile;
        private System.Windows.Forms.Label labelHeartRateValue;
        private System.Windows.Forms.Label labelHeartRate;
        private System.Windows.Forms.Label labelMotorSpeedValue;
        private System.Windows.Forms.Label labelMotorSpeed;
        private System.IO.Ports.SerialPort serialPort;

    }
}

